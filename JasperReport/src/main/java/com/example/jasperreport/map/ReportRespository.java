package com.example.jasperreport.map;

import com.example.jasperreport.entity.Report;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReportRespository extends JpaRepository<Report, Integer> {
}
