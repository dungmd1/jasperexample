package com.example.jasperreport.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Excel2 {
    String stt;
    String code;
    String name;
    String status;
    String projectName;
    String a;
    String b;
    String c;
    String d;
    String e;
    String createDate;
    String endDate3;
    String endDate5;
    String date_complie;
    String endDate6;
    String legalDate;
    String endDate15;
    String f;
    String endDate16;
    String warning;
    String organization;
    String employee16;
    String employee2;
    String note;
}
