package com.example.jasperreport.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Excel1 {
    String total;
    String a;
    String b;
    String c;
}
