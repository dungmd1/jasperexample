package com.example.jasperreport.service;

import com.example.jasperreport.entity.Employee;
import com.example.jasperreport.entity.Report;
import com.example.jasperreport.map.ReportRespository;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ReportService {
    @Autowired
    ReportRespository reportRespository;

    public String exportToWork() throws IOException, JRException {
        List<Report> list = new ArrayList<>();
        list.add(new Report(1,"Mac Dung","ABC"));
        list.add(new Report(2,"Mac Dung2","ABC"));
        list.add(new Report(3,"Mac Dung3","ABC"));
        File file = ResourceUtils.getFile("classpath:totring.jrxml");
        JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
        JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(list);
        Map<String, Object> map = new HashMap<>();
        map.put("title", "Helo anh em");
        map.put("projectName", "Msuite");
        map.put("CollectionParam", dataSource);
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, map, new JREmptyDataSource());
        JRDocxExporter exporter = new JRDocxExporter();
        exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
//        exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(response.getOutputStream()));
//        JasperExportManager.exportReportToPdfFile(jasperPrint, "D:\\settingTeracom\\export"+"\\report.pdf");
        File exportReportFile = new File("D:\\settingTeracom\\export\\report1" + ".docx");
        exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(exportReportFile));
        exporter.exportReport();
//        response.setHeader("Content-Disposition", "attachment;filename= report1.doc");
//        response.setContentType("application/octet-stream");
        exporter.exportReport();

        return "Done";
    }

    public void fillData() {
        List<Report> list = reportRespository.findAll();
        System.out.println("a");
    }

}
