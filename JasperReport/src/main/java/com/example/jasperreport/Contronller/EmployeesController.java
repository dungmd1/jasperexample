package com.example.jasperreport.Contronller;

import com.example.jasperreport.service.EmployeesService;
import net.sf.jasperreports.engine.JRException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.io.FileNotFoundException;
import java.io.IOException;

@Controller
public class EmployeesController {
    @Autowired
    private EmployeesService employeesService;
    @GetMapping("/reporst/{format}")
    public String getEmployess(@PathVariable String format) throws JRException, IOException {
        return employeesService.exportReport(format);
    }
}
