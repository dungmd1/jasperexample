package com.example.jasperreport.Contronller;

import com.example.jasperreport.service.ReportService;
import net.sf.jasperreports.engine.JRException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.io.IOException;

@Controller
public class ReportController {
    @Autowired
    ReportService reportService;


    @GetMapping("/report/getDoc1")
    public ResponseEntity<String> getDoc1(HttpServletResponse response) throws JRException, IOException {
       return new ResponseEntity<>(reportService.exportToWork(), HttpStatus.OK);
    }
}
